**INFO: Aplicação em Wordpress**

* *URL Homologação:* <http://vw-outletsmoustache-hml.ageriservicos.com.br>
* *Diretório App:* /var/deploy/vw-outletmoustache-wordpress-hml
* *Repositório:* git@bitbucket.org:ewertonmoustache/outlet-vw.git

*Observação:* Na task 'GIT | Baixando Branch...', o parâmetro `become_user` foi alterado para `root`.